package com.example.myapplication;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class Food extends RecyclerView.Adapter<Food.VH> {

    String[] time, name, stars, type, cash;

    public Food (String[] time, String[] name, String[] stars, String[] cash, String[] type) {
        this.time = time;
        this.cash = cash;
        this.name = name;
        this.stars = stars;
        this.type = type;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_shop, parent, false));
    }

    @Override
    public int getItemCount() {
        return time.length;
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.time.setText("~" + time[position]);
        holder.name.setText(name[position]);
        holder.type.setText(type[position]);
        holder.stars.setText(stars[position]);
        if (Integer.valueOf(cash[position]) <= 1500) {
            holder.star3.setAlpha(0.4F);
            if (Integer.valueOf(cash[position]) <= 1000) {
                holder.star2.setAlpha(0.4F);
                if (Integer.valueOf(cash[position]) <= 500) {
                    holder.star1.setAlpha(0.4F);
                }
            }
        }
    }

    public class VH extends RecyclerView.ViewHolder {
        TextView time, name, type, stars;
        ImageView star1, star2, star3;
        public VH(@NonNull View itemView) {
            super(itemView);
            time = itemView.findViewById(R.id.time);
            name = itemView.findViewById(R.id.nameofshop);
            type = itemView.findViewById(R.id.type);
            stars = itemView.findViewById(R.id.stars);
            star1 = itemView.findViewById(R.id.one);
            star2 = itemView.findViewById(R.id.two);
            star3 = itemView.findViewById(R.id.three);
        }
    }
}
