package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    //Переменные

    ConstraintLayout back;

    boolean black = false;

    static int position = 0;

    static RecyclerView toolbar, foods;

    static Food adapter;

    static Type ad;

    static String[] time = {"35", "50"}, cash = {"1999", "1233"}, stars = {"4.5", "228"}, name = {"Бульбакинг", "КекДональдс"}, type = {"Бургерная", "Бургерная"};

    static LinearLayout load, menu;

    ImageView check1, check2, check3, check4, check5;

    LinearLayout one, two, three, four, five, menuu;

    Button ok, none;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Задание айдишек
        toolbar = findViewById(R.id.tools);
        foods = findViewById(R.id.foods);
        load = findViewById(R.id.load);
        check1 = findViewById(R.id.imageView7);
        check2 = findViewById(R.id.imageView8);
        check3 = findViewById(R.id.imageView9);
        check4 = findViewById(R.id.imageView10);
        check5 = findViewById(R.id.imageView11);
        menu = findViewById(R.id.menu);
        one = findViewById(R.id.a);
        two = findViewById(R.id.b);
        three = findViewById(R.id.c);
        four = findViewById(R.id.d);
        five = findViewById(R.id.e);
        ok = findViewById(R.id.button2);
        none = findViewById(R.id.button);
        menuu = findViewById(R.id.menuu);
        back = findViewById(R.id.back);

        //Настройка ниж. меню
        menu.setVisibility(View.INVISIBLE);
        check1.setVisibility(View.VISIBLE);
        check2.setVisibility(View.INVISIBLE);
        check3.setVisibility(View.INVISIBLE);
        check4.setVisibility(View.INVISIBLE);
        check5.setVisibility(View.INVISIBLE);

        //Настройка RecView
        adapter = new Food(time, name, stars, cash, type);
        foods.setAdapter(adapter);
        ad = new Type(1);
        toolbar.setAdapter(ad);

        //При нажатии на элементы в нижнем мемню
        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check1.setVisibility(View.VISIBLE);
                check2.setVisibility(View.INVISIBLE);
                check3.setVisibility(View.INVISIBLE);
                check4.setVisibility(View.INVISIBLE);
                check5.setVisibility(View.INVISIBLE);
                ok.setText("Готово");
                Animation animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.someanim);
                animation.setDuration(200);
                none.setAnimation(animation);
            }
        });
        two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check1.setVisibility(View.INVISIBLE);
                check2.setVisibility(View.VISIBLE);
                check3.setVisibility(View.INVISIBLE);
                check4.setVisibility(View.INVISIBLE);
                check5.setVisibility(View.INVISIBLE);
                ok.setText("Показать");
                Animation animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.someanim2);
                animation.setDuration(200);
                none.setAnimation(animation);
            }
        });
        three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check1.setVisibility(View.INVISIBLE);
                check2.setVisibility(View.INVISIBLE);
                check3.setVisibility(View.VISIBLE);
                check4.setVisibility(View.INVISIBLE);
                check5.setVisibility(View.INVISIBLE);
                ok.setText("Показать");
                Animation animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.someanim2);
                animation.setDuration(200);
                none.setAnimation(animation);
            }
        });
        four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check1.setVisibility(View.INVISIBLE);
                check2.setVisibility(View.INVISIBLE);
                check3.setVisibility(View.INVISIBLE);
                check4.setVisibility(View.VISIBLE);
                check5.setVisibility(View.INVISIBLE);
                ok.setText("Показать");
                Animation animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.someanim2);
                animation.setDuration(200);
                none.setAnimation(animation);
            }
        });
        five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check1.setVisibility(View.INVISIBLE);
                check2.setVisibility(View.INVISIBLE);
                check3.setVisibility(View.INVISIBLE);
                check4.setVisibility(View.INVISIBLE);
                check5.setVisibility(View.VISIBLE);
                ok.setText("Показать");
                Animation animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.someanim2);
                animation.setDuration(200);
                none.setAnimation(animation);
            }
        });

        //При нажатии на кнопки в нижнем меню
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Вы нажали на кнопку готово/показать", Toast.LENGTH_SHORT).show();
            }
        });
        none.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check1.setVisibility(View.VISIBLE);
                check2.setVisibility(View.INVISIBLE);
                check3.setVisibility(View.INVISIBLE);
                check4.setVisibility(View.INVISIBLE);
                check5.setVisibility(View.INVISIBLE);
                ok.setText("Готово");
                Animation animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.someanim);
                animation.setDuration(200);
                none.setAnimation(animation);
            }
        });
    }

    //При нажатии на Search
    public void onClick(View v) {
        Toast.makeText(this, "Вы нажали на Search", Toast.LENGTH_SHORT).show();
    }

    //Статичный метод для верхнего RecView
    public static void onChoosed (int pos) {
        position = pos;

        load.setAlpha(0);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                load.setAlpha(1);
            }
        }, 200);

        ad = new Type(pos);
        toolbar.setAdapter(ad);

        if (pos == 1) {
            type[0] = "Бургерная";
            type[1] = "Бургерная";
        } else if (pos == 2) {
            type[0] = "Пиццерия";
            type[1] = "Пиццерия";
        } else if (pos == 3) {
            type[0] = "Лапшичная";
            type[1] = "Лапшичная";
        } else if (pos == 4) {
            type[0] = "Столовая";
            type[1] = "Столовая";
        } else {
            menu.setVisibility(View.VISIBLE);
        }

        adapter = new Food(time, name, stars, cash, type);
        foods.setAdapter(adapter);
    }

    //При нажатии на кнопку сверху слева
    public void onLeftCl(View v) {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.animama);
        animation.setDuration(300);
        menuu.setVisibility(View.VISIBLE);
        menuu.setAnimation(animation);
    }

    //Когда нажали на крестик в левом меню
    public void onSomeCl(View v) {
        menuu.setVisibility(View.INVISIBLE);
    }

    //Когда на тёмную тему нажали
    public void onBlackClicked(View v) {
        if (black) {
            black = false;
            back.setBackgroundResource(R.color.black);
        } else {
            black = true;
            back.setBackgroundResource(R.color.white);
        }
    }
}
