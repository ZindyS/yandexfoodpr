package com.example.myapplication;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.nio.file.Path;

public class Type extends RecyclerView.Adapter<Type.Vh> {

    String[] names = {"0", "Бургеры", "Пицца", "Лапша Вок", "Завтраки"};

    int pos;

    public Type (int pos) {
        this.pos = pos;
    }

    @NonNull
    @Override
    public Vh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Vh(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lay, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final Vh holder, final int position) {
        if (position == pos) {
            holder.lay.setBackgroundResource(R.drawable.corners_b);
        } else {
            holder.lay.setBackgroundResource(R.drawable.corners_a);
        }

        if (position == 0) {
            holder.type.setVisibility(View.INVISIBLE);
            holder.sett.setVisibility(View.VISIBLE);
            holder.sett.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MainActivity.onChoosed(position);
                }
            });
        } else {
            holder.type.setVisibility(View.VISIBLE);
            holder.sett.setVisibility(View.INVISIBLE);
            holder.type.setText(names[position]);
            holder.type.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MainActivity.onChoosed(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return names.length;
    }

    public class Vh extends RecyclerView.ViewHolder {
        ConstraintLayout lay;
        TextView type;
        ImageView sett;
        public Vh(@NonNull View itemView) {
            super(itemView);
            type = itemView.findViewById(R.id.name);
            sett = itemView.findViewById(R.id.imageView3);
            lay = itemView.findViewById(R.id.con);
        }
    }
}
